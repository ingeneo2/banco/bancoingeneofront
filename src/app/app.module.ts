import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CreateClientesComponent } from './components/create-clientes/create-clientes.component';
import {DetallesClienteComponent} from "./components/detalles-cliente/detalles-cliente.component";
import { ActualizarClienteComponent } from './components/actualizar-cliente/actualizar-cliente.component';
import { CrearCuentaComponent } from './components/crear-cuenta/crear-cuenta.component';
import { ListarCuentaComponent } from './components/listar-cuenta/listar-cuenta.component';
import { ActualizarCuentaComponent } from './components/actualizar-cuenta/actualizar-cuenta.component';
import { GenerarInformeComponent } from './components/generar-informe/generar-informe.component';
import { SeleccionarFechasComponent } from './components/seleccionar-fechas/seleccionar-fechas.component';
import { MovimientoComponent } from './components/movimiento/movimiento.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateClientesComponent,
    DetallesClienteComponent,
    ActualizarClienteComponent,
    CrearCuentaComponent,
    ListarCuentaComponent,
    ActualizarCuentaComponent,
    GenerarInformeComponent,
    SeleccionarFechasComponent,
    MovimientoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

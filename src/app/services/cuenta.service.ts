import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Cuenta} from "../models/cuentaRequest";
import {InformeCliente} from "../models/InformeCliente";

@Injectable({
  providedIn: 'root'
})
export class CuentaService {
  apiUrl = 'http://localhost:8081/api/cuentas';

  constructor(private http: HttpClient) { }

  crearCuenta(clienteId: number, cuenta: any): Observable<Cuenta> {
    return this.http.post<Cuenta>(`${this.apiUrl}/${clienteId}`, cuenta);
  }

  obtenerTodasLasCuentas(): Observable<Cuenta[]> {
    return this.http.get<Cuenta[]>(this.apiUrl);
  }

  obtenerCuentaPorId(id: number): Observable<Cuenta> {
    return this.http.get<Cuenta>(`${this.apiUrl}/${id}`);
  }

  actualizarCuenta(id: number, cuenta: any): Observable<Cuenta> {
    return this.http.put<Cuenta>(`${this.apiUrl}/${id}`, cuenta);
  }

  eliminarCuenta(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  generarInformeCliente(clienteId: number, fechaInicio: Date, fechaFin: Date): Observable<InformeCliente> {
    return this.http.get<InformeCliente>(`${this.apiUrl}/${clienteId}/informe`, {
      params: {
        fechaInicio: fechaInicio.toISOString(),
        fechaFin: fechaFin.toISOString()
      }
    });
  }
}

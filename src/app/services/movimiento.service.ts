// movimiento.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Movimiento} from "../models/movimiento";


@Injectable({
  providedIn: 'root'
})
export class MovimientoService {
  private apiUrl = 'http://localhost:8081/api/movimientos';

  constructor(private http: HttpClient) { }

  registrarMovimiento(cuentaId: number, movimiento: Movimiento): Observable<Movimiento> {
    console.log(this.apiUrl, cuentaId, movimiento);
    return this.http.post<Movimiento>(`${this.apiUrl}/${cuentaId}`, movimiento);
  }

  obtenerTodosLosMovimientos(): Observable<Movimiento[]> {
    return this.http.get<Movimiento[]>(this.apiUrl);
  }

  eliminarMovimiento(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }
}

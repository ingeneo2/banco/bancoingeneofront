import { TestBed } from '@angular/core/testing';

import { ChuckNorrisService } from './chuk-norris.service';

describe('ChukNorrisService', () => {
  let service: ChuckNorrisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChuckNorrisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CreateClientesComponent} from "./components/create-clientes/create-clientes.component";
import {DetallesClienteComponent} from "./components/detalles-cliente/detalles-cliente.component";
import {ActualizarClienteComponent} from "./components/actualizar-cliente/actualizar-cliente.component";
import {CrearCuentaComponent} from "./components/crear-cuenta/crear-cuenta.component";
import {ListarCuentaComponent} from "./components/listar-cuenta/listar-cuenta.component";
import {ActualizarCuentaComponent} from "./components/actualizar-cuenta/actualizar-cuenta.component";
import {GenerarInformeComponent} from "./components/generar-informe/generar-informe.component";
import {SeleccionarFechasComponent} from "./components/seleccionar-fechas/seleccionar-fechas.component";
import {MovimientoComponent} from "./components/movimiento/movimiento.component";

const routes: Routes = [

  { path:'', component: CreateClientesComponent },
  { path:'listar-clientes', component: DetallesClienteComponent},
  { path: 'actualizar-cliente/:id', component: ActualizarClienteComponent },
  { path: 'actualizar-cuenta/:id', component: ActualizarCuentaComponent },
  { path: 'crear-cuenta', component: CrearCuentaComponent },
  { path: 'listar-cuenta', component: ListarCuentaComponent },
  { path: 'generar-informe', component: GenerarInformeComponent },
  { path: 'seleccionar-fechas/:id', component: SeleccionarFechasComponent },
  { path: 'movimientos', component: MovimientoComponent },
  { path: '', redirectTo: '/listar-cuentas', pathMatch: 'full' },
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

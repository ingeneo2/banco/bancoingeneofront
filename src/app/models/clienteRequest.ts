export interface ClienteRequest {
  nombre: string;
  direccion: string;
  telefono: string;
  nombreUsuario: string;
  contrasena: string;
  correo: string;
}

export interface Cliente {
  id: number;
  nombre: string;
  direccion: string;
  telefono: string;
}

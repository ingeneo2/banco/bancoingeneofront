import {Cuenta} from "./cuentaRequest";
import {Cliente} from "./clienteRequest";
// generar-informe-cliente.service.ts
import { Observable } from 'rxjs';

export interface InformeCliente {
  cliente: Cliente;
  cuentas: Cuenta[];
  totalDebitos: number;
  totalCreditos: number;
}

export interface GenerarInformeClienteService {
  generarInformeCliente(clienteId: number, fechaInicio: Date, fechaFin: Date): Observable<InformeCliente>;
}

import {Cliente} from "./clienteRequest";

export interface Cuenta {
  id: number;
  numero: string;
  saldo: number;
  cliente: Cliente;
}

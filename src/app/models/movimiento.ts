import {Cuenta} from "./cuentaRequest";

export interface Movimiento {
  id: number;
  tipo: string;
  fecha: Date;
  valor: number;
  cuenta: Cuenta;
}

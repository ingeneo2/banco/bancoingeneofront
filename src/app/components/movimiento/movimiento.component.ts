import { Component } from '@angular/core';
import { Movimiento } from "../../models/movimiento";
import { MovimientoService } from "../../services/movimiento.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registrar-movimiento',
  templateUrl: './movimiento.component.html',
  styleUrls: ['./movimiento.component.css']
})
export class MovimientoComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;

  constructor(private router: Router,private fb: FormBuilder, private movimientoService: MovimientoService) {
    this.RegistroForm = this.fb.group({
      tipo:['', [Validators.required]],
      fecha:['', [Validators.required]],
      valor:['', [Validators.required]],
      cuentaId:['', [Validators.required]],
    })
  }

  registrarMovimiento(): void {
    console.log('Registrando movimiento...');
    this.mostrarErrores = true;
    if (this.RegistroForm.valid) {
      console.log('Registrando movimiento2...');
      const formValues = this.RegistroForm.value;
      let tipo: string = '';
      if (formValues.tipo === 'Depósito') {
        tipo = 'CREDITO';
      } else if (formValues.tipo === 'Retiro') {
        tipo = 'DEBITO';
      }
      const movimiento: Movimiento = {
        id: 0, // Opcional: puedes asignar un valor predeterminado para id si es necesario
        tipo: tipo,
        fecha: formValues.fecha,
        valor: formValues.valor,
        cuenta: {
          id: formValues.cuentaId,
          cliente: formValues.cliente,
          numero: formValues.numero,
          saldo: formValues.saldo,
        }
      };
      this.movimientoService.registrarMovimiento(formValues.cuentaId, movimiento)
        .subscribe(
          (movimientoRegistrado: Movimiento) => {
            console.log('Movimiento registrado:', movimientoRegistrado);
            alert("Realizado exitosamente");
          },
          error => {
            console.error('Error al registrar el movimiento:', error);
          });
    } else {
      console.log("movimiento3")
      this.mostrarErrores = true;
    }

  }

  regresar(): void {
    this.router.navigate(['']);
  }

}

import { Component, OnInit } from '@angular/core';
import {Cliente} from "../../models/clienteRequest";
import {error} from "@angular/compiler-cli/src/transformers/util";
import {ClientesService} from "../../services/clientes.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-detalles-cliente',
  templateUrl: './detalles-cliente.component.html',
  styleUrls: ['./detalles-cliente.component.css']
})
export class DetallesClienteComponent implements OnInit {
  clientes: Cliente[] = [];
  clientesSeleccionados: Cliente[] = [];

  constructor(private router: Router, private clienteService: ClientesService) { }

  ngOnInit(): void {
    this.obtenerClientes();
  }

  obtenerClientes(): void {
    this.clienteService.obtenerTodosLosClientes().subscribe(
      (clientes: Cliente[]) => {
        this.clientes = clientes;
      },
      (error) => {
        console.error('Error al obtener clientes:', error);
        // Manejar el error según sea necesario
      }
    );
  }
  seleccionarCliente(cliente: Cliente): void {
    const index = this.clientesSeleccionados.findIndex(c => c.id === cliente.id);
    if (index === -1) {
      this.clientesSeleccionados.push(cliente);
    } else {
      this.clientesSeleccionados.splice(index, 1);
    }
  }
  actualizarCliente(cliente: Cliente): void {
    if (cliente.id !== undefined) {
      this.clienteService.actualizarCliente(cliente.id, cliente).subscribe(
        () => {
          console.log('Cliente actualizado exitosamente');
          // Puedes realizar alguna acción adicional aquí si es necesario
        },
        (error) => {
          console.error('Error al actualizar cliente:', error);
          // Manejar el error según sea necesario
        }
      );
    }
  }
  irAFormularioActualizacion(id: number): void {
    this.router.navigate(['/actualizar-cliente', id]);
  }
  regresar(): void {
    this.router.navigate(['']);
  }



  eliminarCliente(cliente: Cliente): void {
    if (cliente.id !== undefined) {
      this.clienteService.eliminarCliente(cliente.id).subscribe(
        () => {
          const index = this.clientes.findIndex(c => c.id === cliente.id);
          if (index !== -1) {
            this.clientes.splice(index, 1);
          }
          console.log('Cliente eliminado exitosamente');
        },
        (error) => {
          console.error('Error al eliminar cliente:', error);
        }
      );
    }
  }

}

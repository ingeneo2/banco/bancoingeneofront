import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CuentaService} from "../../services/cuenta.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-crear-cuenta',
  templateUrl: './crear-cuenta.component.html',
  styleUrls: ['./crear-cuenta.component.css']
})
export class CrearCuentaComponent implements OnInit {
  cuentaForm: FormGroup;

  constructor(private router: Router,private fb: FormBuilder, private cuentaService: CuentaService) {
    this.cuentaForm = this.fb.group({
      numero: ['', Validators.required],
      saldo: ['', Validators.required],
      clienteId: ['', Validators.required] // Suponiendo que necesitas el ID del cliente para crear la cuenta
    });
  }

  ngOnInit(): void {
  }

  crearCuenta(): void {
    if (this.cuentaForm && this.cuentaForm.valid) {
      const clienteId = this.cuentaForm.get('clienteId')?.value;
      const cuentaDetails = {
        numero: this.cuentaForm.get('numero')?.value,
        saldo: this.cuentaForm.get('saldo')?.value
      };

      if (clienteId && cuentaDetails.numero && cuentaDetails.saldo) {
        this.cuentaService.crearCuenta(clienteId, cuentaDetails).subscribe(
          respuesta => {
            console.log('Cuenta creada:', respuesta);
            alert("Creado exitosamente");
          },
          error => {
            console.error('Error al crear la cuenta:', error);
          }
        );
      } else {
        console.error('ClienteId, número de cuenta o saldo no válidos.');
      }
    } else {
      console.error('El formulario de cuenta es inválido o nulo.');
    }
  }
  regresar(): void {
    this.router.navigate(['']);
  }
  irListaCuentas(): void {
    this.router.navigate(['/listar-cuenta']);
  }
  irRealizarMovimientos(): void {
    this.router.navigate(['/movimientos']);
  }

}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { InformeCliente } from "../../models/InformeCliente";
import {ChuckNorrisService} from "../../services/chuk-norris.service";

@Component({
  selector: 'app-generar-informe',
  templateUrl: './generar-informe.component.html',
  styleUrls: ['./generar-informe.component.css']
})
export class GenerarInformeComponent implements OnInit {
  informeCliente: InformeCliente | null = null;
  chiste: string = '';
  imageUrl: string = '';
  id: string = '';
  categories: string[] = [];
  constructor(private router: Router, private chuckNorrisService: ChuckNorrisService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // Obtener el informe del estado de la navegación
    const state = window.history.state;
    if (state && state.informeCliente) {
      this.informeCliente = state.informeCliente;
    }
    this.obtenerChisteDeChuckNorris();
  }
  obtenerChisteDeChuckNorris(): void {
    this.chuckNorrisService.obtenerChisteDeChuckNorris()
      .subscribe(
        (data: any) => {
          this.chiste = data.value;
          this.imageUrl = data.icon_url;
          this.id = data.id;
          this.categories = data.categories;
        },
        error => {
          console.error('Error al obtener el chiste de Chuck Norris:', error);
        }
      );
  }

  regresar(): void {
    this.router.navigate(['']);
  }
}

import { Component } from '@angular/core';
import {Cliente} from "../../models/clienteRequest";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ClientesService} from "../../services/clientes.service";

@Component({
  selector: 'app-actualizar-cliente',
  templateUrl: './actualizar-cliente.component.html',
  styleUrls: ['./actualizar-cliente.component.css']
})
export class ActualizarClienteComponent {
  idCliente: number;
  cliente: Cliente;
  formulario: FormGroup;
  mostrarErrores: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private clienteService: ClientesService
  ) {
    this.idCliente=0;
    this.cliente = {
      id: 0,
      nombre: '',
      direccion: '',
      telefono: ''
    };
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      direccion: ['', Validators.required],
      telefono: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.idCliente = this.route.snapshot.params['id'];
    this.obtenerCliente();
    this.inicializarFormulario();
  }

  inicializarFormulario(): void {
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      direccion: ['', Validators.required],
      telefono: ['', Validators.required]
    });
  }

  obtenerCliente(): void {
    this.clienteService.obtenerClientePorId(this.idCliente).subscribe(
      (cliente: Cliente) => {
        this.cliente = cliente;
        this.cargarDatosCliente();
      },
      (error) => {
        console.error('Error al obtener cliente:', error);
        // Manejar el error según sea necesario
      }
    );
  }

  cargarDatosCliente(): void {
    this.formulario.patchValue({
      nombre: this.cliente.nombre,
      direccion: this.cliente.direccion,
      telefono: this.cliente.telefono
    });
  }

  actualizarCliente(): void {
    this.mostrarErrores = true;

    if (this.formulario.valid) {
      const clienteActualizado: Cliente = {
        id: this.idCliente,
        nombre: this.formulario.value.nombre,
        direccion: this.formulario.value.direccion,
        telefono: this.formulario.value.telefono
      };

      this.clienteService.actualizarCliente(this.idCliente, clienteActualizado).subscribe(
        () => {
          console.log('Cliente actualizado exitosamente');
          alert("Cliente actualizado exitosamente");
         this.volverLista()
        },
        (error) => {
          console.error('Error al actualizar cliente:', error);
          // Manejar el error según sea necesario
        }
      );
    }
  }

  volverLista() {
    this.router.navigate(['/listar-clientes']);
  }
}

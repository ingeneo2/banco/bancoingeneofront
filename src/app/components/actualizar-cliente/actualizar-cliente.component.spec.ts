import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { ActualizarClienteComponent } from './actualizar-cliente.component';
import { ClientesService } from '../../services/clientes.service';

describe('ActualizarClienteComponent', () => {
  let component: ActualizarClienteComponent;
  let fixture: ComponentFixture<ActualizarClienteComponent>;
  let routerSpy: jasmine.SpyObj<Router>;
  let clientesServiceSpy: jasmine.SpyObj<ClientesService>;

  beforeEach(() => {
    const routerMock = jasmine.createSpyObj('Router', ['navigate']);
    const clientesServiceMock = jasmine.createSpyObj('ClientesService', ['obtenerClientePorId', 'actualizarCliente']);

    TestBed.configureTestingModule({
      declarations: [ActualizarClienteComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: ActivatedRoute, useValue: { snapshot: { params: { id: 1 } } } },
        { provide: Router, useValue: routerMock },
        { provide: ClientesService, useValue: clientesServiceMock }
      ]
    });

    fixture = TestBed.createComponent(ActualizarClienteComponent);
    component = fixture.componentInstance;
    routerSpy = TestBed.inject(Router) as jasmine.SpyObj<Router>;
    clientesServiceSpy = TestBed.inject(ClientesService) as jasmine.SpyObj<ClientesService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load cliente on init', () => {
    const cliente = { id: 1, nombre: 'Cliente', direccion: 'Dirección', telefono: '123456789' };
    clientesServiceSpy.obtenerClientePorId.and.returnValue(of(cliente));

    fixture.detectChanges();

    expect(clientesServiceSpy.obtenerClientePorId).toHaveBeenCalledWith(1);
    expect(component.cliente).toEqual(cliente);
    expect(component.formulario.value).toEqual(cliente);
  });

  it('should update cliente', () => {
    const cliente = { id: 1, nombre: 'Cliente', direccion: 'Dirección', telefono: '123456789' };
    const updatedCliente = { id: 1, nombre: 'Cliente Modificado', direccion: 'Nueva Dirección', telefono: '987654321' };

    clientesServiceSpy.obtenerClientePorId.and.returnValue(of(cliente));
    clientesServiceSpy.actualizarCliente.and.returnValue(of(updatedCliente));

    fixture.detectChanges();
    component.formulario.patchValue(updatedCliente);
    component.actualizarCliente();

    expect(clientesServiceSpy.actualizarCliente).toHaveBeenCalledWith(1, updatedCliente);
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/listar-clientes']);
  });
});

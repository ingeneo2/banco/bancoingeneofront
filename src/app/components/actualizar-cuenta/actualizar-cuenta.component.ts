import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CuentaService } from '../../services/cuenta.service';
import {Cuenta} from "../../models/cuentaRequest";
import {Cliente} from "../../models/clienteRequest";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-actualizar-cuenta',
  templateUrl: './actualizar-cuenta.component.html',
  styleUrls: ['./actualizar-cuenta.component.css']
})
export class ActualizarCuentaComponent implements OnInit {
  idCuenta: number;
  cuenta: { numero: string; id: number; saldo: number };
  formulario: FormGroup;
  mostrarErrores = false;

  constructor( private router: Router,private route: ActivatedRoute, private fb: FormBuilder, private cuentaService: CuentaService) {
    this.idCuenta = 0;
    this.cuenta = {
      id: 0,
      numero: '',
      saldo: 0
    }
    this.formulario = this.fb.group({
      numero: ['', Validators.required],
      saldo: ['', Validators.required],
      // Otros campos que puedas tener en tu formulario de actualización
    });
  }

  ngOnInit(): void {
    this.idCuenta = this.route.snapshot.params['id'];
    this.obtenerCliente();
    this.inicializarFormulario();
  }
  obtenerCliente(): void {
    this.cuentaService.obtenerCuentaPorId(this.idCuenta).subscribe(
      (cuenta: Cuenta) => {
        this.cuenta = cuenta;
        this.cargarDatosCliente();
      },
      (error) => {
        console.error('Error al obtener cliente:', error);
        // Manejar el error según sea necesario
      }
    );
  }
  inicializarFormulario(): void {
    this.formulario = this.fb.group({
      numero: ['', Validators.required],
      saldo: ['', Validators.required]
    });
  }
  cargarDatosCliente(): void {
    this.formulario.patchValue({
      numero: this.cuenta.numero,
      saldo: this.cuenta.saldo
    });
  }
  actualizarCuenta(): void {
    if (!this.cuenta) {
      console.error('La cuenta no está definida.');
      return;
    }

    this.mostrarErrores = true;

    if (this.formulario.valid) {
      const valoresFormulario = this.formulario.value;
      const cuentaActualizada: { numero: any; id: number; saldo: any } = {
        id: this.cuenta.id,
        numero: valoresFormulario.numero,
        saldo: valoresFormulario.saldo,
      };

      this.cuentaService.actualizarCuenta(this.cuenta.id, cuentaActualizada)
        .subscribe(
          respuesta => {
            console.log('Cuenta actualizada:', respuesta);
            alert("Cuenta actualizada exitosamente");
          },
          error => {
            console.error('Error al actualizar la cuenta:', error);
          }
        );
    } else {
      console.error('El formulario no es válido.');
    }
  }

  volverLista() {
    this.router.navigate(['/listar-cuenta']);
  }

}

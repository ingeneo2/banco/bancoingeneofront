import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InformeCliente } from "../../models/InformeCliente";
import { CuentaService } from "../../services/cuenta.service";

@Component({
  selector: 'app-seleccionar-fechas',
  templateUrl: './seleccionar-fechas.component.html',
  styleUrls: ['./seleccionar-fechas.component.css']
})
export class SeleccionarFechasComponent {
  clienteId: number;
  fechaInicio: string | null = null;
  fechaFin: string | null = null;
  informeCliente: InformeCliente | null = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private generarInformeClienteService: CuentaService
  ) {
    this.clienteId = this.route.snapshot.params['id']; // Capturar el parámetro como 'id'
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      console.log('Params:', params); // Imprime los parámetros para verificar si clienteId está presente
      const id = +params['id']; // Convertir el parámetro a número
      console.log('Params:', params);
      if (!isNaN(id)) {
        this.clienteId = id;
      } else {
        // Manejar el caso en que el parámetro no sea un número válido
        console.error('El clienteId no es un número válido:', params['id']);
      }
    });
  }





  generarInforme(): void {
    const fechaInicioDate = this.fechaInicio ? new Date(this.fechaInicio) : null;
    const fechaFinDate = this.fechaFin ? new Date(this.fechaFin) : null;

    console.log('Fecha de inicio:', fechaInicioDate);
    console.log('Fecha fin:', fechaFinDate);

    // Verifica si las fechas son válidas antes de llamar al servicio
    if (fechaInicioDate && fechaFinDate) {
      // Llama al método del servicio para generar el informe
      console.log(this.clienteId)
      this.generarInformeClienteService.generarInformeCliente(this.clienteId, fechaInicioDate, fechaFinDate)
        .subscribe(
          (informe: InformeCliente) => {
            this.informeCliente = informe;
            console.log('Informe del cliente:', informe);

            // Redirige al componente GenerarInformeComponent con los datos del informe
            this.router.navigate(['/generar-informe'], { state: { informeCliente: informe } });
          },
          error => {
            console.error('Error al generar el informe del cliente:', error);
            // Manejar el error según sea necesario
          }
        );
    } else {
      console.error('Las fechas de inicio y fin son requeridas.');
      // Aquí puedes mostrar un mensaje de error al usuario si lo deseas
    }
  }
}

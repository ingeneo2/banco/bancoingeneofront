import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SeleccionarFechasComponent } from './seleccionar-fechas.component';
import { ActivatedRoute } from '@angular/router';

describe('SeleccionarFechasComponent', () => {
  let component: SeleccionarFechasComponent;
  let fixture: ComponentFixture<SeleccionarFechasComponent>;


  const activatedRouteStub = {
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionarFechasComponent ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarFechasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

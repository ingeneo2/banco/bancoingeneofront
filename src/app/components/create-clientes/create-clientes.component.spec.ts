import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateClientesComponent } from './create-clientes.component';
import {ClientesService} from "../../services/clientes.service";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('CreateClientesComponent', () => {
  let component: CreateClientesComponent;
  let fixture: ComponentFixture<CreateClientesComponent>;
  let clienteService: ClientesService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateClientesComponent],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientTestingModule],
      providers: [ClientesService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateClientesComponent);
    component = fixture.componentInstance;
    clienteService = TestBed.inject(ClientesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call guardar method', () => {
    const guardarSpy = spyOn(component, 'guardar');
    component.guardar();
    expect(guardarSpy).toHaveBeenCalled();
  });



  it('should not call clienteService.crearCliente when form is invalid', () => {
    const invalidFormValues = {
      nombreCliente: '',
      direccionCliente: '',
      telefonoCliente: ''
    };

    const crearClienteSpy = spyOn(clienteService, 'crearCliente');

    component.RegistroForm.setValue(invalidFormValues);
    component.guardar();

    expect(crearClienteSpy).not.toHaveBeenCalled();
  });

  it('should navigate to crear-cuenta route when irCrearCuenta is called', () => {
    const navigateSpy = spyOn(component.router, 'navigate');
    component.irCrearCuenta();
    expect(navigateSpy).toHaveBeenCalledWith(['/crear-cuenta']);
  });

  it('should navigate to listar-clientes route when irListaCliente is called', () => {
    const navigateSpy = spyOn(component.router, 'navigate');
    component.irListaCliente();
    expect(navigateSpy).toHaveBeenCalledWith(['/listar-clientes']);
  });
});

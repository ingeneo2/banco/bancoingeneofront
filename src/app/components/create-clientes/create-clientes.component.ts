import { Component } from '@angular/core';
import {Cliente, ClienteRequest} from "../../models/clienteRequest";
import {ClientesService} from "../../services/clientes.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-clientes',
  templateUrl: './create-clientes.component.html',
  styleUrls: ['./create-clientes.component.css']
})
export class CreateClientesComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(public router: Router, private fb: FormBuilder,private clienteService: ClientesService) {
    this.RegistroForm = this.fb.group({
      nombreCliente: ['', [Validators.required]],
      direccionCliente: ['', [Validators.required]],
      telefonoCliente: ['', [Validators.required]],
    });
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {
      const formValues = this.RegistroForm.value;
      let request: { direccion: any; telefono: any; nombre: any } = {
        nombre: formValues.nombreCliente,
        direccion: formValues.direccionCliente,
        telefono: formValues.telefonoCliente,
      };

      this.clienteService.crearCliente(<Cliente>request).subscribe({
        next: (response) => {
          console.log(response);
          alert("Creado exitosamente");
          this.respuestaCreacion = response;
        },
        error: (error) => {
          console.error('Error en el registro:', error);
          alert("Error al registrar el cliente. Por favor, inténtalo de nuevo.");
          this.mostrarErrores = true;
        }
      });
    } else {
      this.mostrarErrores = true;
    }
  }
  irCrearCuenta(): void {
    this.router.navigate(['/crear-cuenta']);
  }
  irListaCliente(): void {
    this.router.navigate(['/listar-clientes']);
  }
}

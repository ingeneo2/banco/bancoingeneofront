import { Component, OnInit } from '@angular/core';
import {CuentaService} from "../../services/cuenta.service";
import {Cuenta} from "../../models/cuentaRequest";
import {Router} from "@angular/router";
import {InformeCliente} from "../../models/InformeCliente";


@Component({
  selector: 'app-listar-cuenta',
  templateUrl: './listar-cuenta.component.html',
  styleUrls: ['./listar-cuenta.component.css']
})
export class ListarCuentaComponent implements OnInit {
  cuentas: Cuenta[] = [];

  constructor(private router: Router, private cuentaService: CuentaService) { }

  ngOnInit(): void {
    this.obtenerCuentas();
  }

  obtenerCuentas(): void {
    this.cuentaService.obtenerTodasLasCuentas().subscribe(
      cuentas => {
        this.cuentas = cuentas;
      },
      error => {
        console.error('Error al obtener las cuentas:', error);
        // Manejar el error según sea necesario
      }
    );
  }
  actualizarCuenta(cuenta: Cuenta): void {
    this.cuentaService.actualizarCuenta(cuenta.id, cuenta)
      .subscribe(
        respuesta => {
          console.log('Cuenta actualizada:', respuesta);
          // Aquí puedes mostrar un mensaje de éxito o realizar alguna acción adicional
        },
        error => {
          console.error('Error al actualizar la cuenta:', error);
          // Manejar el error según sea necesario
        }
      );
  }
  irAFormularioActualizacion(id: number): void {
    this.router.navigate(['/actualizar-cuenta', id]);
  }
  redirigirAGenerarInforme(): void {
    // Redirigir al usuario al componente GenerarInformeComponent
    this.router.navigate(['/generar-informe']);
  }

  eliminarCuenta(id: number): void {
    this.cuentaService.eliminarCuenta(id)
      .subscribe(
        () => {
          console.log('Cuenta eliminada exitosamente');
          // Actualizar la lista de cuentas después de eliminar
          this.obtenerCuentas();
        },
        error => {
          console.error('Error al eliminar la cuenta:', error);
          // Manejar el error según sea necesario
        }
      );
  }
  generarInforme(): void {
    // Suponiendo que tienes los valores de clienteId, fechaInicio y fechaFin definidos aquí
    const clienteId = 1; // Reemplaza con el valor correspondiente
    const fechaInicio = new Date(); // Reemplaza con el valor correspondiente
    const fechaFin = new Date(); // Reemplaza con el valor correspondiente

    // Llama al método generarInformeCliente pasando los parámetros requeridos
    this.generarInformeCliente(clienteId, fechaInicio, fechaFin);
  }
  generarInformeCliente(clienteId: number, fechaInicio: Date, fechaFin: Date): void {
    this.cuentaService.generarInformeCliente(clienteId, fechaInicio, fechaFin)
      .subscribe(
        (informe: InformeCliente) => {
          // Una vez que se ha generado el informe, redirige al usuario al componente GenerarInformeComponent
          this.router.navigate(['/generar-informe'], { state: { informeCliente: informe } });
        },
        error => {
          console.error('Error al generar el informe del cliente:', error);
          // Manejar el error según sea necesario
        }
      );
  }
  irAFormularioSeleccionarFechas(clienteId: number): void {
    this.router.navigate(['/seleccionar-fechas', clienteId]);
  }
  regresar() {
    this.router.navigate(['/crear-cuenta']);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ListarCuentaComponent } from './listar-cuenta.component';
import { CuentaService } from '../../services/cuenta.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ListarCuentaComponent', () => {
  let component: ListarCuentaComponent;
  let fixture: ComponentFixture<ListarCuentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListarCuentaComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [CuentaService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

# BancoIngeneoFront



## Instalación

Para configurar y ejecutar la aplicación frontend en tu entorno local, sigue estos pasos:

Clona este repositorio en tu máquina local utilizando el siguiente comando:

```
git clone https://gitlab.com/ingeneo2/banco/bancoingeneofront.git
```

Navega hasta el directorio del proyecto:

```
cd bancoingeneofront
```

Instala las dependencias del proyecto utilizando npm:

```
npm install
```

Una vez completada la instalación de las dependencias, puedes iniciar el servidor de desarrollo local ejecutando:

```
npm start
```
La aplicación estará disponible en tu navegador en la siguiente dirección:

```
http://localhost:4200/
```

Una vez que la aplicación esté en funcionamiento, podrás navegar por las diferentes secciones y utilizar todas las funcionalidades proporcionadas.
